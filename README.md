# SHED-C (Severity of HEart Disease Calculator)
## Python Installation
* SHED-C makes use of Python 3.9.4 for the data mining as such it is required to run the code. The conda commands are provided below.
* ```
  conda create -n SHED-C python=3.9.4
  conda activate SHED-C
  pip install -r requirements.txt
  ```
## Directory Structure 
* `Data-Mining-Section`: this where the code for the data mining portion of the project resides
  * Code here can be used to regenerate the classifier 
  * More information about the data mining process can be found in the README in the associated folder
* `Web-application`: this is where the frontend and backend code of the project resides
  * Code here is used to launch the frontend and backend such that the user can interact with the classifier through a GUI
  * More information about running the web application and its code can be found in the associated folder
