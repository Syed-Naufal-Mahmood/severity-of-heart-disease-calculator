import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

                                            ##### Building Outcome Column ####

D = 2
R_AD = 1
OK = 0
EMPTY = 'Empty'


def computeOutcome(row):
    if row['death.within.28.days'] == 1:
        return D  # D_1_MONTH
    elif row['death.within.3.months'] == 1:
        return D  # D_3_MONTH
    elif row['death.within.6.months'] == 1:
        return D  # D_6_MONTH
    elif row['re.admission.within.28.days'] == 1:
        return R_AD  # R_1_MONTH
    elif row['re.admission.within.3.months'] == 1:
        return R_AD  # R_3_MONTH
    elif row['re.admission.within.6.months'] == 1 or row['return.to.emergency.department.within.6.months'] == 1:
        return R_AD  # R_6_MONTH
    elif row['re.admission.time..days.from.admission.'] > 180:
        return R_AD  # R_6_MONTH_PLUS
    else:
        return OK


def createOutcomeColumn(df):
    df['Outcome'] = df.apply(computeOutcome, axis=1)
    return df




                                            ##### Graphing Missing Values ####


# Class for storing attributes and the number of missing values
class attribAndNoValMissing:
    def __init__(self, attribute, noMissing):
        self.attribute = attribute
        self.noMissing = noMissing


# Class to track which figure is being output
class FigNum:
    figNum = 1


def graphNoOfMissingValues(df, nameOfDataSet):

    # Give all empty cells the text 'Empty'
    columnNames = df.columns
    for item in columnNames:
        df[item].replace(np.nan, EMPTY, inplace=True)

    listOfAttributesAndNoMissing = []

    # For every column and for every item in that column if the value is 'Empty'
    # then add that to the counter, create a attribAndNoValMissing instance for this
    # column and place the count for that column in there, append that instance to a list
    for item in columnNames:
        Attribute = df[item]
        counter = 0
        for a in Attribute:
            if a == EMPTY:
                counter = counter + 1

        missingValPerAttributeTracker = attribAndNoValMissing(item, counter)
        listOfAttributesAndNoMissing.append(missingValPerAttributeTracker)
        # print(counter)

    listOfNoMissing = []

    # Output the contents of the list and create a list of just the counts of empty values
    for item in listOfAttributesAndNoMissing:
        # print('Attribute:' , item.attribute, ', ', 'No of empty cells:' , item.noMissing)
        listOfNoMissing.append(item.noMissing)

    # Plot the number of empty values v. the names of each column
    plt.figure(num=FigNum.figNum, figsize=(30, 6))
    axes = plt.subplot()
    axes.bar(columnNames, listOfNoMissing)
    xtickNames = axes.get_xticklabels()
    plt.setp(xtickNames, rotation=90, horizontalalignment='right')
    plt.xlabel('Attributes', fontweight='bold')
    plt.ylabel('No. Missing Values ', fontweight='bold')
    plt.title('Figure ' + str(FigNum.figNum) + ' for ' + nameOfDataSet, fontweight='bold')
    plt.subplots_adjust(bottom=0.35)
    name = nameOfDataSet[:-4]
    plt.savefig('./figures/Data_Cleaning/Figure_' + str(FigNum.figNum) + '_for_' + name + '.png')
    FigNum.figNum = FigNum.figNum + 1
    plt.tight_layout()
    plt.close('all')

    return listOfAttributesAndNoMissing






                                    ##### Analysis For Building Outcomes Column ####


def sumOutcomes(row, listOfColumnsInterestedIn):
    val = 0
    for item in listOfColumnsInterestedIn:
        val += row[item]
    return val


def daysGiven(row, listOfColumnsInterestedIn):
    for item in listOfColumnsInterestedIn:
        if (row[item] != 0):
            return 1
    return 0


def isConsistent(row, listOfColumnsForConsistency):
    if row['sumOfOutcomes'] != 0 and row['daysGiven'] == 0:
        return 'Binary columns given but not Days'
    elif row['sumOfOutcomes'] == 0 and row['daysGiven'] != 0:
        return 'Days given but not Binary Columns'
    else:
        return True


def analysisForOutcomeColumn(testdf):
    # Look at all columns
    columns = testdf.columns
    print(columns)

    # Look at all binary columns to sum over
    listOfColumnsForSum = list(columns[55:61])
    listOfColumnsForSum.append(columns[63])
    print(listOfColumnsForSum)

    # All columns that give a specific number of days
    listOfColumnsForDaysGiven = list(columns[61:63])
    print(listOfColumnsForDaysGiven)

    # Create columns tracking which entries have days given and which entries only rely on the binary columns
    testdf['sumOfOutcomes'] = testdf.apply(sumOutcomes, args=(listOfColumnsForSum,), axis=1)
    testdf['daysGiven'] = testdf.apply(daysGiven, args=(listOfColumnsForDaysGiven,), axis=1)

    # Create a column tracking discrepancies, for example when the number of days are given but the binary columns don't reflect that
    listOfColumnsForConsistency = ['sumOfOutcomes', 'daysGiven']
    testdf['Consistent'] = testdf.apply(isConsistent, args=(listOfColumnsForConsistency,), axis=1)

    # Counting number of discrepancies found
    daysNotGivenCounter = 0
    binaryColumnsNotGivenCounter = 0
    for value in testdf['Consistent']:
        if value == 'Binary columns given but not Days':
            daysNotGivenCounter += 1
        if value == 'Days given but not Binary Columns':
            binaryColumnsNotGivenCounter += 1

    print('The number of times the Binary columns are missing is: ', binaryColumnsNotGivenCounter)
    print('The number of times the Days are missing is: ', daysNotGivenCounter)






                                            ##### Auxiliary Functions ####


def removeRowsWithMissingValues(df):
    noOfRows = df.count()[0]
    IndicesOfRowsToDrop = []
    for i in range(0, noOfRows):
        row = df.iloc[i]
        for attribute in row:
            if attribute == EMPTY:
                IndicesOfRowsToDrop.append(i)
                break

    df = df.drop(IndicesOfRowsToDrop)

    return df


def dropColumns(df, listOfColumnsToDrop):
    for item in listOfColumnsToDrop:
        df.pop(item)

    return df


def replaceNaNs(df):
    df['time.of.death..days.from.admission.'].replace(np.nan, 0, inplace=True)
    df['re.admission.time..days.from.admission.'].replace(np.nan, 0, inplace=True)
    df['return.to.emergency.department.within.6.months'].replace(np.nan, 0, inplace=True)

    return df







                                            ##### Encoding Columns ####


def encodingCategoricalColumns(data):
    # Handle gender
    ge_map = {
        'Male': 0,
        'Female': 1
    }
    data['gender'] = data['gender'].map(ge_map)

    # Handle type.of.heart.failure
    # From some stuff i read, left heart failure can lead to right heart failure. But left heart lead to problem in
    # breathing while right heart lead to swelling of feet
    hf_map = {
        'Right': 1,
        'Left': 0,
        'Both': 2
    }
    data['type.of.heart.failure'] = data['type.of.heart.failure'].map(hf_map)

    # Handle NYHA.cardiac.function.classification
    cc_map = {
        'II': 0,
        'III': 1,
        'IV': 2
    }
    data['NYHA.cardiac.function.classification'] = data['NYHA.cardiac.function.classification'].map(cc_map)

    # Handle Killip.grade
    kg_map = {
        'I': 0,
        'II': 1,
        'III': 2,
        'IV': 3
    }
    data['Killip.grade'] = data['Killip.grade'].map(kg_map)

    # Handle type.II.respiratory.failure
    rf_map = {
        'NonTypeII': 0,
        'TypeII': 1
    }
    data['type.II.respiratory.failure'] = data['type.II.respiratory.failure'].map(rf_map)

    # Handle consciousness
    co_map = {
        'Clear': 0,
        'ResponsiveToSound': 1,
        'ResponsiveToPain': 2,
        'Nonresponsive': 3
    }
    data['consciousness'] = data['consciousness'].map(co_map)

    # Handle respiratory.support.
    # IMV: invasive mask ventilation and NIMV: Non-invasive mask ventilation
    rs_map = {
        'None': 0,
        'IMV': 2,
        'NIMV': 1
    }
    data['respiratory.support.'] = data['respiratory.support.'].map(rs_map)

    # Handle oxygen.inhalation
    oi_map = {
        'OxygenTherapy': 1,
        'AmbientAir': 0
    }
    data['oxygen.inhalation'] = data['oxygen.inhalation'].map(oi_map)

    # Handle ageCat
    # Value is assign to the mean of age range
    ac_map = {
        '(69,79]': 74.0,
        '(79,89]': 84.0,
        '(59,69]': 64.0,
        '(49,59]': 54.0,
        '(89,110]': 99.5,
        '(39,49]': 44.0,
        '(29,39]': 34.0,
        '(21,29]': 25.0
    }
    data['ageCat'] = data['ageCat'].map(ac_map).astype(float)
    return data


#  one hot encoding
def one_hot_encode(data):
    columns = ['gender', 'type.of.heart.failure',
               'NYHA.cardiac.function.classification', 'Killip.grade', 'type.II.respiratory.failure',
               'consciousness', 'respiratory.support.', 'oxygen.inhalation', 'ageCat']
    for attr in columns:
        ohc = pd.get_dummies(data[attr], prefix=attr)
        data = pd.concat([ohc, data], axis=1)
    data = data.drop(columns, axis=1)
    return data

