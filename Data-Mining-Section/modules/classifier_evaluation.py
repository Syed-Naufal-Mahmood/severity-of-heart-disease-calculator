from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


                                        ### Testing Classifier for overfitting ####



def testingOverfitting(train_data, test_data, train_labels, test_labels):

    listOfAccuracyScoresOnTestDataRF = []
    listOfAccuracyScoresOnTrainingDataRF = []
    listOfMinSamplesALeaf = []

    
    for i in range(1,1000,50):

        #Fit classifier under the current conditions  
        rfClassifier = RandomForestClassifier(n_estimators=500, min_samples_leaf= i, class_weight='balanced',
        random_state=1)

        rfClassifier.fit(train_data, train_labels)

        #Make predictions on the test data
        predictions = rfClassifier.predict(test_data)
        successMargin = accuracy_score(test_labels,predictions)
        

        #Make predictions on the training data
        predictions_on_training_Data = rfClassifier.predict(train_data)
        successMarginOnTrainingData = accuracy_score(train_labels,predictions_on_training_Data)

        #maintain lists on the accuracy scores of each
        listOfAccuracyScoresOnTestDataRF.append(successMargin)
        listOfAccuracyScoresOnTrainingDataRF.append(successMarginOnTrainingData)

        listOfMinSamplesALeaf.append(i)

    #Plot the trend that was found
    plt.plot(listOfMinSamplesALeaf, listOfAccuracyScoresOnTestDataRF, marker = 'o')
    plt.plot(listOfMinSamplesALeaf, listOfAccuracyScoresOnTrainingDataRF, marker = 'o')
    plt.xlabel('Min No Of Samples Per Leaf',fontweight = 'bold')
    plt.ylabel('Accuracy',fontweight = 'bold')
    plt.title('Overfitting Statistics For Random Forest')
    plt.legend(['Test Dataset', 'Training Dataset'])
    plt.savefig('./figures/Evaluating_Final_Classifier/Overfitting_Random_Forest.png')
    plt.close('all')


                                #### Plotting Overlap In Feature Values Between Class Labels ####


def plotfeature(df, feature):

    listOfFeatureValuesClass0 = []
    listOfFeatureValuesClass1 = []
    listOfFeatureValuesClass2 = []

    for featureVal, class_label in zip(df[feature], df['Outcome']):

        if class_label == 0:
            listOfFeatureValuesClass0.append(featureVal)

        elif class_label == 1:
            listOfFeatureValuesClass1.append(featureVal)

        elif class_label == 2:
            listOfFeatureValuesClass2.append(featureVal)

    plt.close('all')
    plt.hist(listOfFeatureValuesClass0)
    plt.hist(listOfFeatureValuesClass1)
    plt.hist(listOfFeatureValuesClass2)
    plt.title( 'Distribution of Feature Values on ' + feature)
    plt.legend(['Class OK', 'Class Readmission', 'Class Died'])
    plt.xlabel(feature + ' Values')
    plt.ylabel('Frequency')
    plt.savefig('./figures/Evaluating_Final_Classifier/Distribution_of_Feature_Values_on_' + feature + '.png')
    plt.close('all')
    
