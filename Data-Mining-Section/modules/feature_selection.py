import numpy as np
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFE
from sklearn.feature_selection import RFECV
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import VarianceThreshold
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, ConfusionMatrixDisplay, confusion_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from modules import data_cleaning as clean
from modules import classifier_evaluation as evaluate


# for some reason, importing this class from data_cleaning would mess up the first graph created
class FigNum:
    figNum = clean.FigNum.figNum

                        #### Examining different feature selection methods ####

def remove_low_variance(X):
    selector = VarianceThreshold(threshold=(.8 * (1 - .8)))
    selector = selector.fit(X)
    var_features = X.loc[:, selector.get_support()].columns.tolist()
    drop_fts = np.setdiff1d(X.columns, var_features)
    data = X.drop(drop_fts, axis=1)
    return data


def result_extraction(data, selector):
    mask = selector.get_support()
    selected_features = data.loc[:, mask].columns.tolist()
    return selected_features


def features_selection_rfe(data, outcomes, noFeaturesDesired):
    print('Begin Recursive feature elimination')
    train_data, test_data, train_labels, test_labels = train_test_split(data, outcomes, test_size=0.1, random_state=1)
    rfe_estimator = RandomForestClassifier(random_state=1)
    rfe_selector = RFE(estimator=rfe_estimator, step=20, n_features_to_select=noFeaturesDesired)
    rfe_selector = rfe_selector.fit(train_data, train_labels)
    return result_extraction(train_data, rfe_selector)


def features_selection_rfecv(data, outcomes):
    print('Begin Recursive feature elimination with cross-validation')
    train_data, test_data, train_labels, test_labels = train_test_split(data, outcomes, test_size=0.1, random_state=1)
    rfecv_estimator = RandomForestClassifier(random_state=1)
    rfecv_selector = RFECV(estimator=rfecv_estimator, step=20, cv=StratifiedKFold(2), scoring='accuracy',
                           min_features_to_select=1)
    rfecv_selector = rfecv_selector.fit(train_data, train_labels)
    return result_extraction(train_data, rfecv_selector)


def features_selection_lasso(data, outcomes, noFeaturesDesired):
    print('Begin L1-based feature selection')
    train_data, test_data, train_labels, test_labels = train_test_split(data, outcomes, test_size=0.1, random_state=1)
    lr_estimator = LogisticRegression(penalty='l1', solver='saga', max_iter=100000)
    lr_selector = SelectFromModel(lr_estimator, max_features=noFeaturesDesired)
    lr_selector = lr_selector.fit(train_data, train_labels)
    return result_extraction(train_data, lr_selector)


def feature_selection_tree(data, outcomes, noFeaturesDesired):
    print('Begin tree-based feature selection')
    train_data, test_data, train_labels, test_labels = train_test_split(data, outcomes, test_size=0.1, random_state=1)
    rf_estimator = RandomForestClassifier(random_state=1)
    rf_selectors = SelectFromModel(rf_estimator, max_features=noFeaturesDesired)
    rf_selectors = rf_selectors.fit(train_data, train_labels)
    return result_extraction(train_data, rf_selectors)


def examineDifferentMethods(features, target, noFeaturesDesired):
    # remove variance decreases the accuracy of classifier, so we won't implement that here
    # features = remove_low_variance(features)
    print('Examining different Feature Selection Methods (this may take approx 3 minutes)')
    rfe = features_selection_rfe(features, target, noFeaturesDesired)
    rfecv = features_selection_rfecv(features, target)
    lasso = features_selection_lasso(features, target, noFeaturesDesired)
    tree = feature_selection_tree(features, target, noFeaturesDesired)
    print('Result:')
    train_classifier(features, target, rfe, 'Recursive feature elimination')
    train_classifier(features, target, rfecv, 'Recursive feature elimination cross-validation')
    print(f'Number of features for RFECV: {len(rfecv)} ')
    train_classifier(features, target, lasso, 'L1-based feature selection')
    train_classifier(features, target, tree, 'Tree-based feature selection')


def validation(classifier, data, label, testType, classifierString):
    predictions = classifier.predict(data)
    success_margin = accuracy_score(label, predictions)
    conf_matrix = confusion_matrix(label, predictions)
    display = ConfusionMatrixDisplay(confusion_matrix=conf_matrix)
    display.plot()
    plt.figure(FigNum.figNum)
    plt.title(f'{classifierString} {testType} Confusion Matrix ')
    plt.savefig(f'./figures/Evaluating_Feature_Selection_Methods/{classifierString}_{testType}_confusion_matrix.png')
    FigNum.figNum = FigNum.figNum + 1
    print(f'{classifierString} {testType} accuracy : {success_margin}')


def train_classifier(data, label, model_feature, model_name):
    chosen_features_df = data[model_feature]
    train_data, test_data, train_labels, test_labels = train_test_split(chosen_features_df, label, test_size=0.1,
                                                                        random_state=1)
    rf_classifier = RandomForestClassifier(n_estimators=1000, class_weight='balanced', 
        random_state=np.random.seed(1234))

    rf_classifier.fit(train_data, train_labels)
    validation(rf_classifier, train_data, train_labels, 'train data', model_name)
    validation(rf_classifier, test_data, test_labels, 'test data', model_name)



                            
                ### After examining different Feature Selection methods actually employing RFE ###

# Perform RFE to chose the 10 best features to build our random forest on 
def recursiveFeatureElimination(noFeaturesDesired, train_data, train_labels):
    
    metric = RandomForestClassifier(random_state=1)
    selections = RFE(metric, n_features_to_select=noFeaturesDesired, step=20)
    selections = selections.fit(train_data, train_labels)

    resultsList = selections.get_support()
    featuresSelected = []

    for selected, feature in zip(resultsList, train_data.columns):
        if (selected):
            featuresSelected.append(feature)

    print("Features Selected: ", featuresSelected)
    return featuresSelected


# Build the random forest based on the 10 best features
def createRandomForest(featuresSelected, train_data, test_data, train_labels, test_labels):
    
    #Now only taking the features selected for training
    train_data = train_data[featuresSelected]
    test_data = test_data[featuresSelected]
    
    # Check our model for possibilities of overfitting
    print("Generating Overfitting plot...")
    evaluate.testingOverfitting(train_data, test_data, train_labels, test_labels)

    #Actually fit the final classifier
    rfClassifier = RandomForestClassifier(n_estimators=1000, class_weight='balanced', 
        random_state=np.random.seed(1234))
    rfClassifier.fit(train_data, train_labels)

    #Generate metrics on Training and testing data
    predictionsTestData = rfClassifier.predict(test_data)
    successMarginTesting = accuracy_score(test_labels, predictionsTestData)
    confmatrixTesting = confusion_matrix(test_labels, predictionsTestData)

    predictionsTrainingData = rfClassifier.predict(train_data)
    successMarginTraining = accuracy_score(train_labels, predictionsTrainingData)
    confmatrixTraining = confusion_matrix(train_labels, predictionsTrainingData)

    print("Creating Confusion Matrix on our built Classifier for Training and Testing Data...")
    display = ConfusionMatrixDisplay(confusion_matrix=confmatrixTesting)
    display.plot()
    plt.title( 'RFE-RandomForest Confusion Matrix on the Test Data')
    plt.savefig('./figures/Evaluating_Final_Classifier/final_confusion_matrix_test_data.png')
    plt.close('all')

    display = ConfusionMatrixDisplay(confusion_matrix=confmatrixTraining)
    display.plot()
    plt.title( 'RFE-RandomForest Confusion Matrix on the Training Data')
    plt.savefig('./figures/Evaluating_Final_Classifier/final_confusion_matrix_training_data.png')
    plt.close('all')

    print('Our final Classifier has Training Data Accuracy: ' + str(successMarginTraining))
    print('Our final Classifier has Test Data Accuracy: ' + str(successMarginTesting))

    return rfClassifier
