import pandas as pd
from joblib import dump
from sklearn.model_selection import train_test_split

from modules import classifier_evaluation as evaluate
from modules import data_cleaning as clean
from modules import feature_selection as selection

if __name__ == "__main__":

    print("--   Creating Our Random Forest Classifier   --")
    df = pd.read_csv('dataset/dat.csv')

    ######################################## Cleaning The Data ####################################################
    print(" ")
    print(" -- 1. Data Cleaning Phase -- ")
    print(" ")
    
    # Note there are no NaN's for any of the other binary columns 
    # Replace all the NaN's in the number of days given columns
    print("Remove NaN's in relevant columns...")
    df = clean.replaceNaNs(df)

    #Doing analysis to Gage how to build the outcome column
    # clean.analysisForOutcomeColumn(df)
    
    # create the outcome column
    print("Create Class Label Column...")
    df = clean.createOutcomeColumn(df)

    
    # Drop columns now redundant because of outcome
    listOfColumnsSummarizedByOutcomeColumn = [
        'outcome.during.hospitalization',
        'death.within.28.days',
        're.admission.within.28.days',
        'death.within.3.months',
        're.admission.within.3.months',
        'death.within.6.months',
        're.admission.within.6.months',
        'time.of.death..days.from.admission.',
        're.admission.time..days.from.admission.',
        'return.to.emergency.department.within.6.months',
        'time.to.emergency.department.within.6.months',
    ]

    print("Handling Missing Values...")
    df = clean.dropColumns(df, listOfColumnsSummarizedByOutcomeColumn)

    # Graph the columns and the number of values they each are missing
    listOfColTooManyMissing = []
    listOfColumnsAndNoMissing = clean.graphNoOfMissingValues(df, 'dat.csv')

    # Maintain a record of the columns that are missing too many values and then drop them
    for item in listOfColumnsAndNoMissing:
        if item.noMissing > 100:
            listOfColTooManyMissing.append(item.attribute)

    df = clean.dropColumns(df, listOfColTooManyMissing)

    # Graph the columns and the number of values missing again to show the difference
    clean.graphNoOfMissingValues(df, 'dat.csv')

    # Now remove all rows that are missing values
    df = clean.removeRowsWithMissingValues(df)

    # Graph again to show the difference
    clean.graphNoOfMissingValues(df, 'dat.csv')

    # Dropping columns manually that we know will not be relevant
    print("Drop Columns that we intuitively know we don't want to classify on...")
    listOfColumnsRemoveManually = [
        'Unnamed: 0',
        'inpatient.number',
        'DestinationDischarge',
        'admission.ward',
        'admission.way',
        'discharge.department',
        'visit.times',
        'occupation',
        'dischargeDay',
    ]
    df = clean.dropColumns(df, listOfColumnsRemoveManually)

    # Encoding the categorical columns
    print("Encoding categorical columns..." )
    df = clean.encodingCategoricalColumns(df)

    # Encoding using One Hot Encode
    # df = clean.one_hot_encode(df)

    ######################################## Feature Selection + Build Classifier ####################################################

    # Get outcome vector and unfiltered set of features
    outcomes = df['Outcome']
    data = df.drop(['Outcome'], axis=1)

    noFeaturesDesired = 10

    # Examine the different feature selection methods
    print(" ")
    print(" -- 2. Feature Selection Phase -- ")
    print(" ")
    selection.examineDifferentMethods(data, outcomes, noFeaturesDesired)

    # Use recursive feature elimination to create a random forest
    print(" ")
    print(" -- 3. Now Actually Building and Evaluating Our Classifier -- ")
    print(" ")
    print("Based on the Results Recursive Feature Elimination was Chosen...")
    print ("Building Classifier... ")
    train_data, test_data, train_labels, test_labels = train_test_split(
        data, 
        outcomes, 
        test_size=0.1, 
        random_state=1
        )

    featuresSelected = selection.recursiveFeatureElimination(
        noFeaturesDesired, 
        train_data, 
        train_labels
        )

    print('Drawing Plots on the Distributions of Selected Features per class label...')
    for feature in featuresSelected:
        evaluate.plotfeature(df,feature)

    rfClassifier = selection.createRandomForest(
        featuresSelected, 
        train_data, 
        test_data,
        train_labels,
        test_labels
        )

    print(" ")
    print("The Random forest .pkl file has been created! ")

    dump(rfClassifier, './random_forest.pkl')

