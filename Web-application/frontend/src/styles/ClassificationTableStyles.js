import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    table: {
        height: '20.0rem',
        marginBottom: '2%'
    },
}));

export default useStyles;