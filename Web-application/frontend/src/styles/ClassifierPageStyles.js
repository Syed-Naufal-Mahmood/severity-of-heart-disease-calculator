import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    textField: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: '20rem',
        },
    },
    root: {
        marginTop: 100,
    },
    text: {
        fontSize: '1.4rem'
    },
    subText: {
        fontSize: '1.2rem'
    },
    explanationText: {
        fontSize: '1.0rem'
    },
    classifyButton: {
        color: 'rgb(255,255,255)',
        backgroundColor: 'rgb(0,50,100,1)',
        marginTop: '1.5%',
    },
    csvButton: {
        color: 'rgb(255,255,255)',
        backgroundColor: 'rgb(0,50,100,1)',
    }
}));

export default useStyles;