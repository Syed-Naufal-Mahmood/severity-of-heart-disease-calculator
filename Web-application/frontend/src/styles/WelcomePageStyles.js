import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: '15%',
    },
    text: {
        fontSize: '1.4rem'
    },
    subText: {
        fontSize: '1.0rem',
        marginTop: '1.5%',
    },
    sText: {
        fontSize: '1.0rem',
    },
    startButton: {
        color: 'rgb(255,255,255)',
        backgroundColor: 'rgb(0,50,100,1)',
        marginTop: '1.5%',
    }

}));

export default useStyles;
