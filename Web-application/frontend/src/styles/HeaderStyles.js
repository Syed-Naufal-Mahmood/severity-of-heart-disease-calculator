import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  header: {
    color: 'rgb(255,255,255)',
    backgroundColor: 'rgb(0,50,100,1)',
    alignItems: 'center',
  },
  headerTitle: {
    fontWeight: 'bold', 
    fontSize: '1.6rem'
  }
}));

export default useStyles;
