import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import ClassifierPage from './components/ClassifierPage';
import WelcomePage from './components/WelcomePage'

const App = () => {
  
  return (
    <BrowserRouter>
		<Switch>
    		<Redirect exact from='/' to='/welcome'/>
			  <Route exact path='/welcome'> <WelcomePage/> </Route>
        <Route exact path='/classifier'><ClassifierPage/> </Route>
		</Switch>
    </BrowserRouter>
  );
}

export default App;
