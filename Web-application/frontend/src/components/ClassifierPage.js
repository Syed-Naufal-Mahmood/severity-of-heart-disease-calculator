import React, { useState } from 'react';
import {
    Grid,
    Typography,
    Divider,
    Button
} from '@material-ui/core'
import useStyles from '../styles/ClassifierPageStyles'
import Header from './Header'
import CreateTextFieldForFeature from './CreateTextFieldForFeature'
import DisplayResponse from './DisplayResponse'
import axios from 'axios';
import { CSVReader } from 'react-papaparse'

const ClassifierPage = () => {
    const classes = useStyles();
    const [responseReceived, setResponseReceived] = useState(false);
    const [prediction, setPrediction] = useState([]);
    const features = [
        'id',
        'Urea',
        'Glomerular Filtration Rate',
        'White Blood Cell',
        'Red Blood Cell',
        'Mean Corpuscular Volume',
        'Lymphocyte Count',
        'Platelet',
        'Activated Partial Thromboplastin Time',
        'Prothrombin Activity',
        'Brain Natriuretic Peptide'
    ];
    const buttonRef = React.createRef()
    let initFeatureValues = {};
    features.forEach(feature => {
        initFeatureValues[feature] = 0
    });
    const [featureValues, setFeatureValues] = useState(initFeatureValues);
    const [csvFeatures, setCsvFeatures] = useState([]);
    
    const classify = async (features) => {
        console.log('Features:', features)
        return axios({
            method: "post",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            url: "http://localhost:7888/get_result",
            data:  JSON.stringify({ features: features }),
        }).then((res) => {
            console.log('received:', res.data.result)
            setPrediction(res.data.result);
            setResponseReceived(true)
        });
    };

    const getFile = (e) => {
        if (buttonRef.current) {
            buttonRef.current.open(e);
        }
    }

    const handleOnFileLoad = (data) => {
        const arrays = data.map(data => data.data);
        const featureValues = arrays.map(row => {
            let featureObj = {};
            for (let i = 0; i < row.length; i++) {
                featureObj[features[i]] = row[i]
            }
            return featureObj;
        });
        console.log(featureValues)
        setCsvFeatures(featureValues)
        console.log('csv:', csvFeatures)
        classify(featureValues)
    }

    return (
        <Grid container justify='center' alignItems='center'>
            <Grid item xs={12}>
                <Header />
            </Grid>
            <Grid item xs={12} align='center'>
                <Grid item xs={4} className={classes.root}>
                    <Typography className={classes.text}>
                        Feature Value Input
                    </Typography>
                    <Divider className={classes.divider} orientation='horizontal' />
                </Grid>
            </Grid>
            <Grid item xs={12} align='center'>
                <Grid item xs={4}>
                    <form className={classes.textField} noValidate autoComplete="off">
                        {features.map(feature => (
                            feature !== 'id' ? <CreateTextFieldForFeature feature={feature} featureValues={featureValues} setFeatureValues={setFeatureValues}/> : <div/> 
                        ))}
                    </form>
                </Grid>
            </Grid>
            <Grid item xs={12} align='center'>
                <Button className={classes.classifyButton} onClick={() => {classify([featureValues])}}>Classify</Button>
            </Grid>
            <Grid item xs={12} align='center' >
                <Typography className={classes.subText}>
                    Or
                </Typography>
            </Grid>
            <Grid item xs={12} align='center' >
                <Button component="label" className={classes.csvButton} onClick={getFile}>
                    Classify CSV
                </Button>
                <Grid item xs={1} align='center' >
                    <CSVReader
                        ref={buttonRef}
                        onFileLoad={handleOnFileLoad}
                        noClick
                        noDrag
                    />
                </Grid>
                <Typography className={classes.explanationText}>
                        CSV values should be in the order of:
                        <br/>
                        ID, Urea, Glomerular Filtration Rate, White Blood Cell, Red Blood Cell, Mean Corpuscular Volume,
                        <br/>
                        Lymphocyte Count, Platelet, Activated Partial Thromboplastin Time, Prothrombin Activity, Brain Natriuretic Peptide.
                        <br/>
                        The ID is an arbitrary value so the prediction can easily be matched up with the CSV row
                </Typography>
            </Grid>
            <Grid item xs={12}>
                {responseReceived === true ? <DisplayResponse prediction={prediction} /> : <div />}
            </Grid>
        </Grid>
    );
}

export default ClassifierPage;