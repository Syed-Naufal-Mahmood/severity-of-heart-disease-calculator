import React from 'react';
import {
    TextField,
} from '@material-ui/core'

const CreateTextFieldForFeature = (props) => {
    const {feature, featureValues, setFeatureValues} = props;
    const featureUnits = {
        'Urea': '(mmol/L)',
        'Glomerular Filtration Rate': '(ml/min/m^2)',
        'White Blood Cell': '(10^9/L)',
        'Red Blood Cell': '(10^2/L)',
        'Mean Corpuscular Volume': '(fL)',
        'Lymphocyte Count': '(10^9/L)',
        'Platelet': '(fL)',
        'Activated Partial Thromboplastin Time': '(s)',
        'Prothrombin Activity': '(%)',
        'Brain Natriuretic Peptide': '(pg/mL)'
    };

    const getTextFieldValue = (e) => {
        const textFieldId = e.target.id;
        const value = e.target.value;
        const temp = featureValues;
        temp[textFieldId] = value;
        setFeatureValues(temp);
    }

    return(
        <TextField id={feature} label={feature + featureUnits[feature]} variant='outlined' onChange={getTextFieldValue} type="number"/>
    );
}

export default CreateTextFieldForFeature;