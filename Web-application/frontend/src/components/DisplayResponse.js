import React from 'react';
import {
    Grid,
    Typography,
    Divider,
} from '@material-ui/core'
import useStyles from '../styles/ClassifierPageStyles'
import ClassificatonTable from './ClassificationTable'

const DisplayResponse = (props) => {
    const {prediction} = props;  
    const classes = useStyles();
    const predictionText = [
        'Patient okay for discharge.',
        'Chronic disease possible. Ensure patient is aware of next steps or symptoms to watch out for.',
        'Chances of severe illness and death are high release with extreme caution.'
    ];

    return(
        <React.Fragment>
        <Grid item xs={12} align='center'>
            <Grid item xs={4} className={classes.root}>
                <Typography className={classes.text}>
                    Prediction
                </Typography>
                <Divider className={classes.divider} orientation='horizontal' />
            </Grid>
            <Grid item xs={6} className={classes.table}>
                <ClassificatonTable predictions={prediction}/>
            </Grid>
        </Grid>
        </React.Fragment>
    );
}

export default DisplayResponse;