import React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import useStyles from '../styles/ClassificationTableStyles'

const ClassificatonTable = (props) => {
    const {predictions} = props;
    const classes = useStyles();
    const predictionText = [
        'Patient okay for discharge',
        'Chronic disease possible ensure patient is aware of next steps or symptoms to watch out for',
        'Chances of severe illness and death are high release with extreme caution'
    ];
    const columns = [
        { field: 'id', headerName: 'Patient ID', width: 120 },
        { field: 'classification', headerName: 'Prediction', width: 800}
    ]

    const rows = predictions.map(prediction => {
        const val = {id: prediction.id, classification: predictionText[prediction.classification]};
        return val;
    });

    return(
        <DataGrid columns={columns} rows={rows} pageSize={20} className={classes.table}/>
    );
}

export default ClassificatonTable; 