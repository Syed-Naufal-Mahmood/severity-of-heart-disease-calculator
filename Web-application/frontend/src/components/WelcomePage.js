import React from 'react';
import {
    Grid,
    Typography,
    Button
} from '@material-ui/core'
import useStyles from '../styles/WelcomePageStyles'
import Header from './Header'
import { useHistory } from "react-router-dom";

const WelcomePage = () => {
    const classes = useStyles();
    const history = useHistory();

    const onButtonClick = () => {
        history.push("/classifier/");
    }

    return(
       <Grid container justify='center' alignItems='center'>
           <Grid item xs={12}>
                <Header/>
           </Grid>
           <Grid item xs={12} className={classes.root}>
               <Typography align='center' className={classes.text}>
                    Welcome to SHED-C
                    <br/>
                    Use it to determine the likelihood of re-admission for a patient with heart disease. 
                    <br/>
                    It makes predictions based on the following parameters:
                    <br/>
                    Urea, Glomerular Filtration Rate, White Blood Cell, Red Blood Cell, Mean Corpuscular Volume,
                    <br/>
                    Lymphocyte Count, Platelet, Activated Partial Thromboplastin Time, Prothrombin Activity, Brain Natriuretic Peptide.
                    <br/>
                    These parameters were determined to give the highest accuracy from the dataset that the classifier was trained on.
               </Typography>
           </Grid>
           <Grid item xs={12} align='center'>
                <Button variant='contained' size='large' className={classes.startButton} onClick={onButtonClick}>
                    Start
                </Button>
           </Grid>
           <Grid item xs={12}>
                <Typography align='center' className={classes.subText}>
                    By
               </Typography>
               <Typography align='center' className={classes.sText}>
                    Syed Naufal Mahmood, Dev Arora, Murphy Vu, and Samuel Leong
               </Typography>
           </Grid>
       </Grid>
    );
}

export default WelcomePage;