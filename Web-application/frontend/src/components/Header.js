import React from 'react';
import {
    Typography,
    AppBar,
    Toolbar
} from '@material-ui/core'
import useStyles from '../styles/HeaderStyles'

const Header = () => {
    const classes = useStyles()

    return(
        <AppBar className={classes.header}>
            <Toolbar>
                <Typography component='div' align='center' className={classes.headerTitle}>
                    Severity of Heart Disease Calculator (SHED-C)
                </Typography>
            </Toolbar>
        </AppBar>
    
    );
}

export default Header;