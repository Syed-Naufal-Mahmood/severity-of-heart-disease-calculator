# SHED-C (Severity of HEart Disease Calculator) 

## Installation and Setup Instructions
* npm is required to run the frontend, it can be download at the link:
  * https://www.npmjs.com/get-npm
* Run `./build.sh` to start the frontend and backend, a URL will be displayed showing where the app is hosted
  * The build script calls the whatever Python instance is in the path, as such ensure it is the same one for which the requirements were installed
  * Ensure the Python instance in the path is the same one for which the requirements was installed
  * If using conda, run `build.sh` in the Anaconda prompt with the `SHED-C` environment activated 
* If for whatever reason the build script won't work the user can also run the following commands:
  * to start the frontend:
    * ```
      cd ./frontend
      npm install
      npm install -g serve //ensure serve is installed
      npm run build
      ```
  * to start the backend:
    * ```
      python server.py
      ```
    * Ensure the Python being used has the requirements installed 

## Directory Layout
* Within the directory there is `server.py` which is the backend for the web application 
  * `examples.csv` provides a few input values for the classifier when running the frontend 
  * `build.sh` the aforementioned build script
* `./frontend` contains all of the code used to build the frontend 
  * `./frontend/src/components` contains the javascript files which create everything seen in the UI 
    * This includes files for each page, input fields, page header, and the prediction table
  * `./frontend/src/styles` contains the javascript files that apply styling to the pages (such as colours or component placement on the page)
    * each file is associated with the file of the same name in `./frontend/src/components`
  * `./frontend/src` contains the files that puts everything together for the web pages and the navigation between pages

## UI 
* The UI was designed on a 27" monitor, as such it looks best on a similarly sized monitor, a screenshot is shown below of the UI
  ![UI](./screenshots/UI.PNG)
