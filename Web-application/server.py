from flask import Flask, request, jsonify
from wsgiserver import WSGIServer
from joblib import load
from flask_cors import CORS, cross_origin
import os

app = Flask(__name__, static_folder="build", template_folder="build")
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

rfClassifier = None

# This assumes the classifier uses these in this order
fields = [
  "Urea",
  "Glomerular Filtration Rate",
  "White Blood Cell",
  "Red Blood Cell",
  "Mean Corpuscular Volume",
  "Lymphocyte Count",
  "Platelet",
  "Activated Partial Thromboplastin Time",
  "Prothrombin Activity",
  "Brain Natriuretic Peptide",
]


@app.route("/get_result", methods=["POST"])
@cross_origin()
def main():
  result = list()
  data = request.json["features"]
  print(data)
  for row in data:
    features = [float(row[field]) for field in fields]
    print(features)

    classification = rfClassifier.predict([features])
    print(classification)
    result.append({'id': int(row['id']), 'classification': int(classification[0])})
    print(classification)

  print(result)

  return jsonify({"result": result})


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
  if path and os.path.exists(os.path.join(app.static_folder, path)):
    return app.send_static_file(path)
  return app.send_static_file("index.html")


if __name__ == "__main__":
  from argparse import ArgumentParser
  parser = ArgumentParser()
  parser.add_argument("--port", type=int, default=7888)
  parser.add_argument("--classifier", type=str, default="./random_forest.pkl")
  args = parser.parse_args()

  print("Loading classifier from", args.classifier)
  rfClassifier = load(args.classifier)

  print("Listening on port", args.port)
  WSGIServer(app, port=args.port).start()
